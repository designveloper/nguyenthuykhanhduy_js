# Javascript

## Introduction

* JavaScript is a scripting language that runs in the browser and interacts with the HTML Markup and CSS Rules to change what you see and what you can do.

* jQuery is a library for JavaScript functions and commands that allows us to do fancy JavaScript things with less code.

* ECMAScript itself is not a language but an evolving standard.

## The basics

* The browser is literally rendering the page from the top down
```
<sciprt src="<filename>" /> (The HTML will parse until it encounters a reference to a JavaScript, then the JavaScript file is loaded, executed and then we continue the HTML parsing)

<sciprt src="<filename>" async /> (The JS file will be downloaded alongside the HTML and render blocking only kicks in when the script is executed)

<sciprt src="<filename>" defer /> (Loading and execution of the JS file, only happens when everything else is loaded)
```

- How to write good JS

	+ JS is case sensitive
	
	+ Use camelCase
	
	+ Whitespace matters

	+ End each statement with a semicolon

## Working with data

### Variables

* var is optional. var puts a variable in local scope. If a variable is defined without var, it is in global scope

### Data types

* Numeric: regular numbers and integers
* String: strng of letters and symbols
* Boolean
* null: store the intentional absence of a value
* undefined
* Symbol

### Works with strings and numbers

```
var a = 4;
var b = "5";
var c = "ducks";
console.log(a + b) // return "45"
console.log(a - b) // return -1

console.log(a + c) // return "4ducks"
console.log(a - c) // return NaN
```

### Conditional statements and logic
```
== (pretty lenient)
=== (absolute strict equality)
```

### Arrays
* Each value in the array can be a separate data type.

### Properties and methods in array
* push: add elemenet to array and returns the new array length
* pop: Removes the last element from array and returns the value that was "popped out"
* shift: Shifting is equivalent to popping, working on the first element instead of the last and returns the string that was "shifted out"
* slice: slices out a piece of an array into a new array
* joins all elements of an array into a string 
* splice: changes the contents of an array by removing existing elements and/or adding new elements.

## Functions and Objects

### Basic function
```
function functionName() {

}
```

### Anonymous functions
```
var functionName = function() {

}
``` 

### Immediately invoked function expressions
```
var result = (function() {

})();
```

### Variable scope
* When you declare a variable in the root of your script, so, independently, outside of any function, it becomes a global variable you can access from anywhere within the script.
* When you declare a variable inside a function, it becomes a local variable.

### let and const
* const (Can't be changed once defined)
* let (block scope)

### Closures
* A function inside a function, but relies on variables from the outside function to work

## Javascript and the DOM: Changing DOM Elements

### DOM (document object model)
* Is the model of the document that forms the current webpage
* Modelling the relationships between the different nodes

### DOM with selector methods
```
document.getElementById (return element with specific ID)

document.getElementsByClassName (get all elements with a specific class name as array)

document.geElementsByTagName (get all elements with a specific HTML tag as array)

document.querySelector (get first element matching specific selector)

document.querySelectorAll (get all elements matching specific selector)
```

### Access and change elements
* innerHTML: the html inside
* outerHTML: include the htmt tag

### Access and change classes
```
document.querySelector().classList.<add/remove/item/toggle/contain/replace> (add, remove... classes).
```

### Access and change attributes
```
hasAttribute
getAttribute
setAttribute
removeAttribute
```

### Add new DOM element
* 1. Create the element
* 2. Create the text node that goes inside the element
* 3. Add the text node to the element
* 4. Add the element to the DOM tree

### Add Inline CSS to an element
```
document.querySelector().style.[CSS property]
```
* Overrides whatever CSS is applied to an element
* On the other hand, style is also an attribute -> The above methods for attribute can be used
* In most cases, best practice is to create CSS rules and use JS to manage these classes to apply the rules to the element

## Javascript and the DOM: Events

### Typical events
* Common Browser-Level Events
    
    + Load: when the resource and dependents have finished loading

    + Error: when a resource failed to load
		
	+ Online/Offline: seft-explanatory
		
	+ Resize: when a viewpoint is resized
		
	+ Scroll: when a viewpoint is srolled up/down/left/right
* Common DOM Events
    + Focus: when an element receives focus (clicked, tabbed to, etc.)
		
	+ Blur: when an element loses focus (leaving form field, etc.)
		
	+ Resets/Submit: form-specific events
		
	+ Mouse events: click, mouseover, drag, drop, etc.
* Other events:
    + Media events: relate to audio/video playback
		
    + Progress events: monitor ongoing browser progress
		
    + CSS transition events: transition start/run/end
	
## Loops
* Break: terminate the current loop, jump to the next stament

* Continue: terminate the current iteration of the loop, jump back up and run the next iteration.

## Troubleshooting, Validating, and Minifying JavaScript

### Script linting
* Online tools

	* JSLint

	* JSHint
	
* Local automated tools

	* ESLint

* Minifying:

	* Online tool: http://www.minifier.org/

	* Local automated: Uglify